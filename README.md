# webpack-express-boilerplate
A basic react project template


#Key technologies
React - Display components
Redux - Central state store
Thunk - Middleware for Redux that allows async reducer methods
Bootstrap - CSS / Layout
Node - JS Server
Express - web server

Bebel - transpiler
Webpack - build and bundle
Gulp - task helper
ESLint - syntac and style check
Karma - test runner
Jasmine - test helper
SASS - css helper

Code structure based on the REDUX structure from https://github.com/yildizberkay/redux-example/tree/master/src
Project structure from http://www.christianalfoni.com/articles/2015_04_19_The-ultimate-webpack-setup

Test packages
Enzyme react test helper - https://github.com/airbnb/enzyme
Chai assertion library - http://chaijs.com/guide/installation/
Chai as promised - https://github.com/domenic/chai-as-promised
Chai-enzyme assets - https://github.com/producthunt/chai-enzyme
Dirty Chai lint friendly asserts - https://www.npmjs.com/package/dirty-chai
Sinon mock / spy - http://sinonjs.org/

 
