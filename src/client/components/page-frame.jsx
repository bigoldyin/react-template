/* eslint-disable no-unused-vars */

import React from 'react'
import PageHeader from '../containers/page-header.jsx' 

/* eslint-enable no-unused-vars */

require('./stylesheets/page-frame.scss')

export const PageFrame = ({ children }) => {
  return <div className='pageframe'>
    <PageHeader></PageHeader>
    <div className='row'>            
      {children}
    </div>
  </div>
}

export default PageFrame
