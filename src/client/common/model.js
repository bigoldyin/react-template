
const Falcor = require('falcor')
const FalcorHttpDataSource = require('falcor-http-datasource')

const model = new Falcor.Model({source: new FalcorHttpDataSource('/api/model.json') });
export default model;


    // const falcor =
    // new Model({
    //   cache: {
    //     todos: [
    //         { $type: 'ref', value: ['todosById', 44] },
    //         { $type: 'ref', value: ['todosById', 54] },
    //         { $type: 'ref', value: ['todosById', 97] }
    //     ],
    //     todosById: {
    //       '44': {
    //         name: 'get milk from corner store',
    //         done: false,
    //         prerequisites: [
    //             { $type: 'ref', value: ['todosById', 54] },
    //             { $type: 'ref', value: ['todosById', 97] }
    //         ]
    //       },
    //       '54': { name: 'withdraw money from ATM', done: false },
    //       '97': { name: 'pick car up from shop', done: false }
    //     }
    //   }
    // })
