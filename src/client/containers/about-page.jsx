/* eslint-disable no-unused-vars */

import React, { Component, PropTypes } from 'react'
import * as types from '../common/app-action-types'
import { Link } from 'react-router'

/* eslint-enable no-unused-vars */

export class AboutPage extends Component {
  render () {
    return (
      <div className='pagebody'>
        <div className='col-xs-12'>
          <p>About Page</p>
          <Link to='/'>Home</Link>
        </div>
      </div>
    )
  }
}

export default AboutPage

