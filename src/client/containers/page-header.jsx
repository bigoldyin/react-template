/* eslint-disable no-unused-vars */

import React, { Component, PropTypes } from 'react'
require('./stylesheets/page-header.scss')

/* eslint-enable no-unused-vars */

import { connect } from 'react-redux'

class PageHeader extends Component {
  
  static propTypes = {
    status: PropTypes.string.isRequired
  }
    
  render () {
    let {status} = this.props
   
    return <div className='row pageheader' >
      <div className='col-xs-10'>
        <h1 className='pagetitle'>{'Fixed for now'}</h1>
      </div>
      <div className='col-xs-2'>
        <p className='pageheader__statustext'>{status}</p>
      </div>
    </div>
  }
}

export default connect(state => ( { 
    status: state.app.status
  }))(PageHeader)