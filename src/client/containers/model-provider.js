import { Component, PropTypes, Children } from 'react'

export default class ModelProvider extends Component {
  constructor(nextProps, context) {
    super(nextProps, context)
    this.model = nextProps.model
    this.actions = nextProps.actions
  }
  
  static childContextTypes = {
    model: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired
  }

  getChildContext() {
    return { model: this.model, actions: this.actions }
  }
  
  render() {
    return this.props.children;
  }
}
