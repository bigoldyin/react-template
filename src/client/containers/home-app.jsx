/* eslint-disable no-unused-vars */

import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import * as action from '../actions/home-app-actions'
import Styles from './stylesheets/home-app.scss'
import PageFrame from '../components/page-frame.jsx'

/* eslint-enable no-unused-vars */

export class HomeApp extends Component {
  render () {
    return (
      <PageFrame>
        {this.props.children || 'No Content'}
      </PageFrame>
    )
  }
}

export default connect()(HomeApp)
