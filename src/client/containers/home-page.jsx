/* eslint-disable no-unused-vars */

import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Styles from './stylesheets/home-page.scss'
import * as types from '../common/app-action-types'
import { Link } from 'react-router'

/* eslint-enable no-unused-vars */

export class HomePage extends Component {
  
  constructor() {
    super();
  }
  
  static contextTypes = { 
    model: PropTypes.object.isRequired,
    actions: PropTypes.object
   };

  state = {
      greeting: 'not set'
  };
  
  componentWillMount() {
    const model = this.context.model
    model.get("greeting").then((response) => {
      this.setState({ greeting : response.json.greeting }) 
    })     
  }
  
  doStatusChange = () => {
    const actions = bindActionCreators(this.context.actions, this.props.dispatch)
    actions.updateStatus()
  }
  
  render () {
    let {route, params, location } = this.props
    return (
      <div className='pagebody'>
        <div className='col-xs-12'>
          <p name='greeting'>Greeting: {this.state.greeting || 'not set'}</p>
          <p><Link to='/about'>About</Link></p>
          <button className='btn btn-primary' type='button' onClick={this.doStatusChange} >Update Status</button>
        </div>
      </div>
    )
  }
}

export default connect()(HomePage)