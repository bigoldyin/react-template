/* eslint-disable no-unused-vars */

import React, { Component } from 'react'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import reducer from '../reducers/index.js'
import Styles from './stylesheets/home-app'
import thunk from 'redux-thunk'
import { Router, Route, Link, browserHistory, IndexRoute } from 'react-router'
import HomeApp from './home-app.jsx'
import HomePage from './home-page.jsx'
import AboutPage from './about-page.jsx'
import ModelProvider from './model-provider.js'
import model from '../common/model.js'

import * as actions from '../actions/home-app-actions'

/* eslint-enable no-unused-vars */

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore)
const store = createStoreWithMiddleware(reducer)

export default class App extends Component {
  render () {
    return (
      <Provider store={store}>  
        <ModelProvider model={model} actions={actions}>
          <Router>
            <Route name='Home Page' path='/' component={HomeApp}>
              <IndexRoute name='Home Page' component={HomePage} />
              <Route name='About Page' path='/about' component={AboutPage}></Route>
            </Route>
          </Router>
        </ModelProvider>
      </Provider>
    ) 
  }
}
