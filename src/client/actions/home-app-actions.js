import * as types from '../common/app-action-types'
import getStatus from '../api/test'

export function setStatus (status) {
  return (dispatch, getState) => {
    dispatch({
      type: types.SET_STATUS,
      status: status
    })
  }
}

export function updateStatus () {
  return (dispatch, getState) => {
    dispatch({
      type: types.SET_STATUS,
      status: `Updating from server`
    })
  
  
    return getStatus(dispatch)
  }
}

export { getStatus }
