import * as types from '../common/app-action-types'

const appReducer = (state = { status: 'Starting' }, action) => {
  switch (action.type) {
    case types.SET_STATUS:
      return {
        ...state,
        status: action.status
      }

    default:
      return state
  }
}

export default appReducer
