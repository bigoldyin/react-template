export default function getStatus (callback) {
  return fetch('http://localhost:3000/api/v1/status')
    .then((response) => response.json())
    .then((data) => callback(data))
    .catch((err) => {
      return Promise.reject(err)
    })
}
