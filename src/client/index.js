/* eslint-disable no-unused-vars */
require('jquery')
require('bootstrap-loader')
import React from 'react'
import ReactDOM from 'react-dom'
import App from './containers/app.jsx'

/* eslint-enable no-unused-vars */

ReactDOM.render(<App />, document.getElementById('root'))

