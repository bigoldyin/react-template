
"use strict";
const MongoClient = require('mongodb').MongoClient;

class Dal {
  constructor(connectionString, collectionName) {
    this.connectionString = connectionString
    this.collectionName = collectionName
    this.client = require('mongodb').MongoClient
  }

  addDoc(doc) {
    return new Promise((resolve, reject) => {
      this.client.connect(this.connectionString, (err, db) => {
        if(err) {
          reject(Error(err))
          return
        }            

        let collection = db.collection(this.collectionName)
        collection.insert(doc, {w:1}, (err, result) => {
            err ? reject(Error(err)) : resolve(result.ops[0])
        })
      })
    })
  }
 
  connect(connectionString) {
    
   this.client.connect(this.connectionString, function(err, db) {
      if(!err) {
        console.log("We are connected");
        
        let collection = db.collection('test');
        let doc = {id:1, fieldtoupdate:1};
        collection.insert(doc, {w:1}, (err, result) => {
          if(!err) {
            console.log('Inserted document')
          }
          
          collection.update({id:1}, {$set:{fieldtoupdate:2}}, {w:1}, (err, result) =>{
            if(!err) {
              console.log('Updpated document')
            }  
          });
          
          collection.findOne({id:1}, (err, item) => {
              if(!err) {
                console.log('fetched document', item)
              }  
          });
          
          collection.remove({id:1}, {w:1}, (err, result) => {
              if(!err) {
                console.log('removed document')
              }  
          });

          collection.remove();
        });
      } else {
        throw(err)
      }
    }) 
  }
}

module.exports = Dal