const falcorExpress = require('falcor-express');
const Router = require('falcor-router');
const Dal = require('../dal/datastore.js');

const dal = new Dal("mongodb://localhost:27017/exampleDb", "test");

dal.addDoc({id:1, data:'this data'})
  .then((result)=> console.log('added doc', result))
  .catch((err)=> console.log('error on add doc', err))
  
  

const modelRoute = falcorExpress.dataSourceRoute(function (req, res) {
  // create a Virtual JSON resource with single key ("greeting")
  return new Router([
    {
      // match a request for the key "greeting"    
      route: "greeting",
      
      // respond with a PathValue with the value of "Hello World."
      get: function() {
        return {path:["greeting"], value: "Hello World Again"};
      }
    }
  ]);
});

module.exports = modelRoute;
