var express = require('express')
var router = express.Router()

router.get('/v1/status', function (req, res) {
  res.json({
    type: 'SET_STATUS',
    status: `Server status ${(new Date()).getSeconds()}`
  })
})

module.exports = router
