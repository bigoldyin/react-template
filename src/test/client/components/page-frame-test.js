/* eslint-env jasmine */
import React from 'react'
import {mount, render, shallow} from 'enzyme'
import chai from 'chai'
import chaiEnzyme from 'chai-enzyme'

import PageFrame from 'src/components/page-frame.jsx'
import PageHeader from 'src/containers/page-header.jsx'

chai.should();
chai.use(chaiEnzyme()) 

describe('<PageFrame />', () => {
  
  it('renders page header', () => {
    const wrapper = shallow(<PageFrame />)
    wrapper.find('.pageframe').should.be.present()
  })
  
  it('renders children', () => {
    const wrapper = shallow(<PageFrame ><div id='child'></div></PageFrame>)
    wrapper.find('#child').should.be.present()
  })
  
  it('renders page header', () => {
    const wrapper = shallow(<PageFrame />)
    wrapper.should.contain(<PageHeader/>)
  })
})
