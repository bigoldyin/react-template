/* eslint-env jasmine */

import React from 'react'
import {mount, render, shallow} from 'enzyme'
import chai from 'chai'
import chaiEnzyme from 'chai-enzyme'

import { PageFrame } from 'src/components/page-frame.jsx'
import { HomeApp } from 'src/containers/home-app.jsx'

chai.should();
chai.use(chaiEnzyme()) 

describe('<HomeApp />', () => {

  it('renders page frame', () => {
    const wrapper = shallow(<HomeApp />)
    
    wrapper.should.contain(PageFrame)
  })
  
  it('renders passed child', () => {
    const testClass = 'testChild'
    const wrapper = shallow(<HomeApp><div className={testClass}></div></HomeApp>)
  
    wrapper.should.contain(<div className="testChild" />)
  })
})
