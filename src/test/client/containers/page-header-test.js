/* eslint-env jasmine */
import React from 'react'
import {mount, render, shallow} from 'enzyme'
import chai from 'chai'
import chaiEnzyme from 'chai-enzyme'

import PageHeader from 'src/containers/page-header.jsx'

chai.should();
chai.use(chaiEnzyme()) 

describe('<PageHeader />', () => {
  const defaultStatus = 'mockstatus'
  const mountoptions = { 
    context : { 
      store : {
        subscribe : sinon.stub(),
        getState : sinon.stub().returns({ app : { status : defaultStatus} }),
        dispatch : sinon.stub()
      }
    }
  }
  
  it('renders page header', () => {
    const wrapper = mount(<PageHeader />, mountoptions)
    wrapper.find('.pageheader').should.be.present()
  })

  it('renders status', () => {
    const wrapper = mount(<PageHeader />, mountoptions)
    wrapper.find('.pageheader__statustext').text().should.equal(defaultStatus)
  })
})
