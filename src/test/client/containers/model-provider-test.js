/* eslint-env jasmine */
import React, { Component, PropTypes } from 'react'
import {mount, render, shallow} from 'enzyme'
import chai from 'chai'
import chaiEnzyme from 'chai-enzyme'
import chaiAsPromised from 'chai-as-promised'
import sinonStubPromise from 'sinon-stub-promise';

sinonStubPromise(sinon);
chai.use(chaiAsPromised);
chai.should();
chai.use(chaiEnzyme()) 

import ModelProvider from 'src/containers/model-provider.js'
import * as actions from 'src/actions/home-app-actions'

describe('<ModelProvider/>', () => {
  const model = { data : 'test'}
  
  it('Renders children', () => {
    const wrapper = shallow(<ModelProvider model={model} actions={actions}><div/></ModelProvider>)
    wrapper.should.contain(<div/>)
  })
  
  it('has model context type', () => {
    ModelProvider.childContextTypes.model.should.equal(PropTypes.object.isRequired)
  })
  
  it('has actions context type', () => {
    ModelProvider.childContextTypes.actions.should.equal(PropTypes.object.isRequired)
  })
})
