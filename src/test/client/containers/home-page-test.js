/* eslint-env jasmine */

import React from 'react'
import {mount, render, shallow} from 'enzyme'
import chai from 'chai'
import chaiEnzyme from 'chai-enzyme'
import ModelProvider from 'src/containers/model-provider.js'
import chaiAsPromised from 'chai-as-promised'
import sinonStubPromise from 'sinon-stub-promise';

sinonStubPromise(sinon);
chai.use(chaiAsPromised);
chai.should();
chai.use(chaiEnzyme()) 

import { HomePage } from 'src/containers/home-page.jsx'
import * as types from 'src/common/app-action-types'

describe('<HomePage />', () => {
  const defaultGreeting = 'Default greeting'
  const resultData = {
    json : { 
      greeting : defaultGreeting
    }
  }
  const promise = sinon.stub().returnsPromise().resolves(resultData);
  const model = { get : sinon.mock() }
  const actions = { updateStatus : sinon.mock() } 
  const mountOptions =  { context : { model : model, actions : actions}}

  beforeEach(()=>{
      model.get.withArgs('greeting').atLeast(1).returns(promise());
  })
  
  it('renders page page body', () => {
    const wrapper = shallow(<HomePage />, mountOptions)
    wrapper.find('.pagebody').should.be.present()
  })
  
  it('calls model for greeting', () => {
    const wrapper = shallow(<HomePage />, mountOptions)
    
    model.get.verify();
  })
  
  it('shows not set greeting when state not set', () => {
    const wrapper = shallow(<HomePage />, mountOptions)

    wrapper.setState( { greeting : undefined } )
    
    wrapper.find('p').first().should.be.text('Greeting: not set')
  })

  it('shows greeting from model', () => {
    const wrapper = shallow(<HomePage />, mountOptions)
    
    wrapper.find('p').first().should.be.text('Greeting: ' + defaultGreeting)
  })
  
  it('click test', () => {
    const dispatchProp = sinon.mock()
    
    actions.updateStatus.once()
       
    const wrapper = shallow(<HomePage />, mountOptions)
    wrapper.setProps( { dispatch : dispatchProp } )

    wrapper.find('button').simulate('click')
    
    dispatchProp.verify()
    actions.updateStatus.verify()
  })
})
