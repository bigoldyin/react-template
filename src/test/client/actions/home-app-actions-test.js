/* eslint-env jasmine */
import React, { Component, PropTypes } from 'react'
import {mount, render, shallow} from 'enzyme'
import chai from 'chai'
import chaiEnzyme from 'chai-enzyme'
import chaiAsPromised from 'chai-as-promised'
import sinonStubPromise from 'sinon-stub-promise'
require('whatwg-fetch')
import "babel-polyfill"

sinonStubPromise(sinon)
chai.use(chaiAsPromised)
chai.should()
chai.use(chaiEnzyme()) 

import * as actions from 'src/actions/home-app-actions'
import { getStatus } from 'src/actions/home-app-actions'
import * as types from 'src/common/app-action-types'

describe('home-app-actions', () => {
  const response = `{ "type": "${types.SET_STATUS}", "status": "server mock" }`
      
  let resultPromise
  let fetchStub
  
  beforeEach(() => {
    const resultData = new window.Response( response, {
      status: 200,
      headers: {
        'Content-Type':'application/json'
      }
    })
    resultPromise = Promise.resolve(resultData)
    fetchStub = sinon.stub(window, 'fetch')
    fetchStub.returns(resultPromise)     
  })

  afterEach(() => {
    window.fetch.restore();
  })
  
  describe('setStatus', ()=>{
    
    it('returns function', ()=>{
      let res = actions.setStatus()
      res.should.be.a('function')
    })
    
    it('function calls dispatch with correct params', ()=>{
      const status = 'teststatus'
      const mockDispatch = sinon.mock()
      mockDispatch.withArgs({ 
        type: types.SET_STATUS,
        status : status 
      }).once()

      let res = actions.setStatus(status)
      res(mockDispatch,null)
      
      mockDispatch.verify()
    })
  })
  
  describe('updateStatus', ()=>{

    it('sets status to updating from server before calling', (done)=>{
      const mockDispatch = sinon.spy()
           
      let res = actions.updateStatus()
      let serverpromise = res(mockDispatch,null)
      serverpromise.then(()=>{
        mockDispatch.calledWith({ type: 'SET_STATUS', status: `Updating from server`}).should.be.true
      })
     
      serverpromise.should.eventually.be.fulfilled.notify(done)
    })
    
    it('sets status from server result', (done)=>{
      const mockDispatch = sinon.spy()
    
      let res = actions.updateStatus()
      let serverpromise = res(mockDispatch,null)
      serverpromise.then(()=>{
        mockDispatch.calledWith({type: 'SET_STATUS', status: 'server mock'}).should.be.true  
      })
     
      serverpromise.should.eventually.be.fulfilled.notify(done)
    })
    
    it('calls expected url', (done)=>{
      const mockDispatch = sinon.spy()
    
      let res = actions.updateStatus()
      let serverpromise = res(mockDispatch,null)
      fetchStub.withArgs('http://localhost:3000/api/v1/status').called.should.be.true
      serverpromise.should.eventually.be.fulfilled.notify(done)
    })
    
    it('throws error', (done)=>{
      const mockDispatch = sinon.stub()
      mockDispatch.onSecondCall().throws('test error')
      
      let res = actions.updateStatus()
      let serverpromise = res(mockDispatch,null)
      
      serverpromise.catch((err)=>{
        err.name.should.equal('test error')
      })
      
      serverpromise.should.eventually.be.rejected.notify(done)
    })
  })
})
