require('webpack')
var path = require('path')

module.exports = function (config) {
  config.set({
    frameworks: ['jasmine', 'chai', 'sinon', 'sinon-stub-promise'], // use the jasmine test framework
    files: [
      'tests.webpack.js' // just load this file
    ],
    preprocessors: {
      'tests.webpack.js': ['webpack']
    },
    reporters: ['dots', 'html', 'coverage'], // report results in this format
    coverageReporter: {
      instrumenterOptions: {
        istanbul: { noCompact: true }
      },
      reporters: [
        {type: 'html', subdir: 'report-html'},
        {type: 'text-summary'}
      ],
      dir: 'coverage' // path to created html doc,
    },
    webpack: { // kind of a copy of your webpack config
      debug:   true,
      devtool: 'source-map',
      babel: {
        presets: ['es2015', 'stage-0', 'react']
      },
      // *optional* isparta options: istanbul behind isparta will use it
      isparta: {
        embedSource: true,
        noAutoWrap: true,
        // these babel options will be passed only to isparta and not to babel-loader
        babel: {
          presets: ['es2015', 'stage-0', 'react']
        }
      },
      externals: {
        'cheerio': 'window',
        'react/lib/ExecutionEnvironment': true,
        'react/lib/ReactContext': true
      },
      module: { 
        noParse: [
            /node_modules\/sinon/,
        ],
        preLoaders: [
          // transpile all files except testing sources with babel as usual
          {
            test: /\.jsx$|\.js$/,
            exclude: [
              path.resolve('src/'),
              path.resolve('node_modules/')
            ],
            loader: 'babel'
          },
          // transpile and instrument only testing sources with isparta
          {
            test: /\.jsx$|\.js$/,
            include: path.resolve('src/client'),
            loader: 'isparta'
          }
        ],
        loaders: [
          {
            test: /\.jsx$|\.js$/,
            exclude: /node_modules/,
            loader: 'babel',
            query: {
              'presets': ['react', 'es2015', 'stage-0']
            }
          },

          // the loaders will be applied from right to left
          {
            test: /\.scss$/,
            loaders: ['style', 'css', 'sass']
          }, {
            test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$|\.eot$|\.woff2$/,
            loader: 'file'
          }
        ]
      },
      resolve: {
        root: path.resolve(__dirname),
        alias: {
          src: '../../../client'
        },
        extensions: ['', '.js', '.jsx', '.scss']
      }
    },
    webpackServer: {
      noInfo: true // please don't spam the console when running in karma!
    }
  })
}
