const gulp = require('gulp')
const webpack = require('webpack-stream')
const named = require('vinyl-named')
const del = require('del')
const Server = require('karma').Server
const path = require('path')

gulp.task('build', function () {
  return gulp.src(['./src/client/index.js', './src/server/server.js'])
    .pipe(named())
    .pipe(webpack(require('./webpack.config.js')))
    .pipe(gulp.dest('dist/'))
})

gulp.task('build:production', function () {
  return gulp.src(['./src/client/index.js', './src/server/server.js'])
    .pipe(named())
    .pipe(webpack(require('./webpack.production.config.js')))
    .pipe(gulp.dest('dist/'))
})

gulp.task('test:once', function (done) {
  new Server({
    configFile: path.join(__dirname, 'karma-conf.js'),
    singleRun: true,
    browsers: ['PhantomJS']
  }, done).start()
})

gulp.task('test:tdd', function (done) {
  new Server({
    configFile: path.join(__dirname, 'karma-conf.js'),
    singleRun: false,
    browsers: ['Chrome']    
  }, done).start()
})

gulp.task('clean', function () {
  return del([
    // here we use a globbing pattern to match everything inside the `builds` folder
    'dist/**/*'
  ])
})

